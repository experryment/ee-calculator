# Equal Experts Pocket Calculator

React application which displays an interactive Equal Experts branded pocket calculator for challenge version **0c6e95aff1b2f3a1d2dbfdc2f9957e567b554159**

## Get up and running
Please enter the following commands to clone the repository and get the app up and running:

```sh
> git clone https://bitbucket.org/experryment/ee-calculator.git
> cd ee-calculator
> npm install
> npm start
```
This should automatically open a browser window for you at [http://localhost:3000](http://localhost:3000) to view the application in action

## Run the tests
```sh
npm test a
npm test -- --coverage
```

## Available Features
- Add +
- Subtract -
- Multiply x
- Divide ÷
- Clear

## Potential Additional features
- Decimal Point .
- Square Root √
- Remainder %
- Positive/Negative +/-
- On/Off
- Keyboard integration

## Notes
Typical Use Case:

* Enter a value using any number keys
* Select an operator symbol + - / x
* Enter a second value using any number keys
* Press the = key

Currently supports one calculation at a time  
Please press **AC** button to clear the display before attempting another calculation