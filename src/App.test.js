import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Equal Experts Pocket Calculator',() => {

    describe('KeyPad', ()=> {
        let calculator, clearBtn, btn1, btn8;

        beforeEach(() => {
            calculator = mount(<App/>);
        })

        it('should update display with the relevant value of the selected digit', ()=> {
            btn1 = calculator.find(`#digit1`);
            btn1.simulate('click');
            const display = calculator.find('input[type="text"]');
            expect(display.prop('value')).toBe('1');
        })

        it('should display the relevant value of multiple digits pressed', ()=> {
            btn1 = calculator.find(`#digit1`);
            btn8 = calculator.find(`#digit8`);
            btn1.simulate('click');
            btn8.simulate('click');
            const display = calculator.find('input[type="text"]');
            expect(display.prop('value')).toBe('18');
        })

        it('should display the relevant value of multiple digits pressed', ()=> {
            btn1 = calculator.find(`#digit1`);
            btn8 = calculator.find(`#digit8`);
            btn1.simulate('click');
            btn8.simulate('click');
            const display = calculator.find('input[type="text"]');
            expect(display.prop('value')).toBe('18');
        })

        it('should clear the display', ()=> {
            clearBtn = calculator.find(`#clearAC`)
            clearBtn.simulate('click');
            const display = calculator.find('input[type="text"]');
            expect(display.prop('value')).toBe('');
        })

    })
})
