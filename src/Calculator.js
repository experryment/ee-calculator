const Calculator = {
    '+' : (value1, value2) => {
        return (value1 + value2);
    },

    '-' : (value1, value2) => {
        return value1 - value2;
    },

    '÷' : (value1, value2) => {
        return value1 / value2;
    },

    'x' : (value1, value2) => {
        return value1 * value2;
    }
}

export default Calculator;