import React, { Component } from 'react'
import KeyPadButton from './KeyPadButton';
import Calculator from './Calculator';

class App extends Component {

    state = {
        display : '',
        digits : [7,8,9,4,5,6,1,2,3,0],
        symbols: ['+','-','÷','x'],
        activeSymbol : '',
        persist : false,
        memory : 0
    }

    onDigitSelected = (value) => {
        this.updateDisplay(value, this.state.persist);
        if(this.state.persist) {
            this.setState({
                persist : false
            })
        }
    }

    onSymbolSelected = (value) => {
        this.setState({
            memory : this.state.memory + parseInt(this.state.display,10),
            activeSymbol : value,
            persist : true
        })
    }

    onEqualsSelected = () => {
        if ( this.state.memory && this.state.display ){
            const calculation = Calculator[this.state.activeSymbol](this.state.memory, parseInt(this.state.display,10));
            this.updateDisplay( calculation, true );
        }
    }

    onPointSelected = () => {
        //decimal point
    }

    onClearSelected = () => {
        this.clear();
    }

    updateDisplay = ( value, clear ) => {
        const updated = clear? `${value}` : `${this.state.display}${value}`;
        
        this.setState({
            display : updated
        })
    }

    clear = () => {
        this.setState({
            memory : 0,
            activeSymbol : '',
            display: ''
        })
    }

    render() {
        return <div className="calculator">
                <div className="logo">
                    <img src="logo.svg" alt="" width="130" />
                </div>

                <div className="solar"></div>

                <div className="display">
                    <input type="text" className="screen" disabled value={this.state.display} />
                </div>

                <div className="buttons">

                    <ul className="digits">
                        {
                            this.state.digits.map( digit => {
                                return <li className="button" key={digit}><KeyPadButton type="digit" value={digit} onSelected={this.onDigitSelected}/></li>
                            })
                        }
                        <li className="button"><KeyPadButton type="point" value="." onSelected={this.onPointSelected}/></li>
                        <li className="button"><KeyPadButton type="clear" value="AC" onSelected={this.onClearSelected}/></li>
                    </ul>

                    <ul className="symbols">
                        {
                            this.state.symbols.map( (symbol,index) => {
                                return <li key={symbol} className="symbol button">
                                    <KeyPadButton type="symbol" label={index} value={symbol} onSelected={this.onSymbolSelected}/>
                                </li>
                            })
                        }
                    </ul>
                    <div className="equals button" onClick={this.onEqualsSelected}>
                        <div>
                            <img src="logo-equals.svg" width="50" alt="" />
                        </div>
                    </div>
                </div>

            </div>
    }

}

export default App;