import React, { Component } from 'react';

class KeyPadButton extends Component {
    onSelected = () => {
        this.props.onSelected(this.props.value)
    }
    render() {
        const label = this.props.label !== undefined ? this.props.label : this.props.value;
        return (
            <button id={`${this.props.type}${label}`} className={this.props.type} value={this.props.value} onClick={this.onSelected}>{this.props.value}</button>
        )
    }
}

export default KeyPadButton;

