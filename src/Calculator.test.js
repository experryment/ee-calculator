import Calculator from './Calculator';

describe('Equal Experts Pocket Calculator',() => {

    describe('Calculator', () => {

        it('should correctly add two positive values', () => {
            expect(Calculator['+'](1,2)).toEqual(3)
        })

        it('should correctly subtract two positive values', () => {
            expect(Calculator['-'](1,2)).toEqual(-1)
        })

        it('should correctly divide two positive values', () => {
            expect(Calculator['÷'](1,2)).toEqual(0.5)
        })

        it('should correctly multiply two positive values', () => {
            expect(Calculator['x'](1,2)).toEqual(2)
        })


        it('should correctly add two negative values', () => {
            expect(Calculator['+'](-1,-2)).toEqual(-3)
        })

        it('should correctly subtract two negative values', () => {
            expect(Calculator['-'](-1,-2)).toEqual(1)
        })

        it('should correctly divide two negative values', () => {
            expect(Calculator['÷'](-1,-2)).toEqual(0.5)
        })

        it('should correctly multiply two negative values', () => {
            expect(Calculator['x'](-1,-2)).toEqual(2)
        })


        it('should correctly add positive and negative values', () => {
            expect(Calculator['+'](-1,2)).toEqual(1)
        })

        it('should correctly subtract positive and negative values', () => {
            expect(Calculator['-'](-1,2)).toEqual(-3)
        })

        it('should correctly divide positive and negative values', () => {
            expect(Calculator['÷'](-1,2)).toEqual(-0.5)
        })

        it('should correctly mutiply positive and negative values', () => {
            expect(Calculator['x'](-1,2)).toEqual(-2)
        })


        it('should correctly add decimal values', () => {
            expect(Calculator['+'](0.5,0.5)).toEqual(1)
        })

        it('should correctly subtract decimal values', () => {
            expect(Calculator['-'](0.5,0.5)).toEqual(0)
        })

        it('should correctly divide decimal values', () => {
            expect(Calculator['÷'](0.5,0.5)).toEqual(1)
        })

        it('should correctly mutiply decimal values', () => {
            expect(Calculator['x'](0.5,0.5)).toEqual(0.25)
        })

    })

})